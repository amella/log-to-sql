__author__ = 'andres'

from  wirelessidea.com.repository import Connection

report_types= {}

report_types['HTTP_HOURLY'] = "INSERT INTO mt_traffic_reports.httpxbihourlyreport(id, hour, bill_success, bill_failure, bill_total, bill_success_rate, success, failures, total_mt, success_rate) VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"
report_types['HTTP_SUMMARY'] = "INSERT INTO mt_traffic_reports.httpxbihourlyreportsummary(id, total_bill, total_bill_success, total_content, total_pending_content, bill_succes_rate, avg_tph, avg_tps, speed_status) VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s)"
report_types['SMPP_HOURLY'] = "INSERT INTO mt_traffic_reports.smpphourlyreport(id, hour, success, failures, total_mt, success_rate) VALUES (%s,%s ,%s ,%s ,%s ,%s)"
report_types['SMPP_SUMMARY'] = "INSERT INTO mt_traffic_reports.smpphourlyreportsummary(id, total_bill, total_bill_success, total_content, total_pending_content, bill_success_rate, avg_mph, avg_mps, speed_status) VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s)"


def insert_reports(report_type, reports):

    cnx= None
    cursor= None

    try:

        cnx = Connection.get_connection()
        cursor = cnx.cursor()

        #for report in reports:
         #   foo=report_types[report_type]

          #  print foo
           # print report
        cursor.executemany(report_types[report_type], reports)

        cnx.commit()

    except:

            raise

    finally:

        if not cursor is None:
            cursor.close()

        if not cnx is None:
            cnx.close()