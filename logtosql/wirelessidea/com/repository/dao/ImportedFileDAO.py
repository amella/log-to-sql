__author__ = 'andres'

from  wirelessidea.com.repository import Connection


def exists_imported_file(data):

    imported_file_count = -1

    count_dml = "select count(*) from  mt_traffic_reports.importedfile where id_countryCarrier=%s and route=%s and generated_date=%s;"

    cnx = None
    cursor= None

    try:

        cnx = Connection.get_connection()

        if cnx is not None:

            cursor = cnx.cursor()

            cursor.execute(count_dml, data)

            for k in cursor:
                imported_file_count = k[0]
                break

            cnx.commit()

    except:

            raise

    finally:

        if not cursor is None:
            cursor.close()

        if not cnx is None:
            cnx.close()


    return 0 != imported_file_count


def insert_imported_file(data):

    imported_file_id = -1

    insert__imported_file_dml = "INSERT INTO mt_traffic_reports.importedfile(id_countryCarrier, route, imported_date, generated_date) VALUES  (%s,%s, now(),%s);"

    cnx = None
    cursor= None

    try:

        cnx = Connection.get_connection()

        if cnx is not None:

            cursor = cnx.cursor()

            cursor.execute(insert__imported_file_dml, data)

            cnx.commit()

            imported_file_id = cursor.lastrowid

    except:

            raise

    finally:

        if not cursor is None:
            cursor.close()

        if not cnx is None:
            cnx.close()

    return imported_file_id