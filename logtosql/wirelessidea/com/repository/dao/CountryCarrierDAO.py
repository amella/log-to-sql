__author__ = 'andres'

from  wirelessidea.com.repository import Connection


def insert_or_retrieve_country_carrier(country_code, carrier_short_name):

    existCarrierDDL = "SELECT COUNT(id) FROM mt_traffic_reports.carrier WHERE short_name= %s"
    existCountryDDL = "SELECT COUNT(id) FROM mt_traffic_reports.country WHERE code = %s"
    existCountryCarierDDL = "SELECT COUNT(id) FROM mt_traffic_reports.countrycarrier where id_carrier=%s and id_country=%s"

    insertCarrierDML = "INSERT INTO mt_traffic_reports.carrier(short_name) VALUES (%s);"
    insertCountryDML = "INSERT INTO mt_traffic_reports.country(code) VALUES (%s);"
    insertCountryCarrierDML = "INSERT INTO mt_traffic_reports.countrycarrier(id_carrier,id_country) VALUES (%s ,%s);"

    idCarrierDDL = "SELECT id FROM mt_traffic_reports.carrier WHERE short_name=%s"
    idCountryDDL = "SELECT id FROM mt_traffic_reports.country WHERE code=%s"
    idCountryCarrierDDL = "SELECT id FROM mt_traffic_reports.countrycarrier WHERE id_carrier=%s and id_country=%s"

    id_country = -1
    id_carrier = -1
    id_country_carrier = -1

    cnt = 0
    cnx = None
    cursor = None

    try:

        cnx = Connection.get_connection()

        if cnx is not None:

            cursor = cnx.cursor()

            cursor.execute(existCountryDDL, (country_code, ))

            for w in cursor:
                cnt = w[0]

            cursor.close()

            if cnt == 0:
                cursor = cnx.cursor()
                cursor.execute(insertCountryDML, (country_code, ))
                cursor.close()

            cursor = cnx.cursor()
            cursor.execute(existCarrierDDL, (carrier_short_name, ))

            for w in cursor:
                cnt = w[0]

            cursor.close()

            if cnt == 0:
                cursor = cnx.cursor()
                cursor.execute(insertCarrierDML, (carrier_short_name, ))
                cursor.close()

            cursor = cnx.cursor()
            cursor.execute(idCarrierDDL, (carrier_short_name, ))

            for w in cursor:
                id_carrier = w[0]

            cursor.close()

            cursor = cnx.cursor()
            cursor.execute(idCountryDDL, (country_code, ))

            for w in cursor:
                id_country = w[0]

            cursor.close()

            cursor = cnx.cursor()
            cursor.execute(existCountryCarierDDL, (id_carrier, id_country, ))

            for w in cursor:
                cnt = w[0]

            cursor.close()

            if cnt == 0:
                cursor = cnx.cursor()
                cursor.execute(insertCountryCarrierDML, (id_carrier, id_country, ))
                cursor.close()

            cursor = cnx.cursor()
            cursor.execute(idCountryCarrierDDL, (id_carrier, id_country, ))

            for w in cursor:
                id_country_carrier = w[0]

            cursor.close()

            cnx.commit()

    except:

            raise

    finally:

        if not cursor is None:
            cursor.close()

        if not cnx is None:
            cnx.close()

    return id_country_carrier