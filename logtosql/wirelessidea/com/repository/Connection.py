__author__ = 'andres'

import mysql
import mysql.connector
import mysql.connector.pooling
from mysql.connector import errorcode

db_config = {
    "database": "mt_traffic_reports",
    "user": "mt_traffic",
    "password": "KqC5V4159y",
    "host": "127.0.0.1",
    "charset" : "utf8"
}

pool = mysql.connector.pooling.MySQLConnectionPool(pool_name="log_to_sql_pool", pool_size=10,
                                                          **db_config)


def get_connection():
    cnx = None

    try:

        cnx = pool.get_connection()

    except mysql.connector.Error as err:

        raise err

    return cnx
