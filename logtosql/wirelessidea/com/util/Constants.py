__author__ = 'andres'


class Constants(object):
    HYPHEN_CHAR = '-'
    DOT_CHAR = '.'
    HOURLY = 'Hourly'
    SUMMARY = 'Summary'
    EMPTY_CHAR = ''
    COMMA_CHAR = ','
    HTTP_XBI_LINE_LEN = 13
    SMPP_LINE_LEN = 9
    NULL_STRING = 'null'
    MIN_YEAR = 2013