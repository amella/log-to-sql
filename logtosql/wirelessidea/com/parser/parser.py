__author__ = 'andres'


import os
import csv

from wirelessidea.com.repository.dao import CountryCarrierDAO, ImportedFileDAO, ReportDAO
from wirelessidea.com.util.Constants import Constants


class Parser(object):

    def __init__(self, route):

        self._route = route

    def get_data(self, imported_file, items):

        data = items

        if not data[4] is None:
            data[4] = data[4].replace(Constants.COMMA_CHAR, Constants.EMPTY_CHAR)

        if not data[5] is None:
            data[5] = data[6].replace(Constants.COMMA_CHAR, Constants.EMPTY_CHAR)

        if not data[6] is None:
            data[6] = data[6].replace(Constants.COMMA_CHAR, Constants.EMPTY_CHAR)

        if not data[7] is None:
            data[7] = data[7].replace(Constants.COMMA_CHAR, Constants.EMPTY_CHAR)

        if not data[9] is None:
            data[9] = data[9].replace(Constants.COMMA_CHAR, Constants.EMPTY_CHAR)

        if not data[10] is None:
            data[10] = data[10].replace(Constants.COMMA_CHAR, Constants.EMPTY_CHAR)

        data[11] = data[11][0]

        return (imported_file, data[4], data[5], data[6], data[7], data[8], data[9], data[10],data[11],)


    def generate(self):

        for folder, subs, files in os.walk(self._route):

            for filename in files:

                id_hyphen = filename.find(Constants.HYPHEN_CHAR)
                id_dot = filename.find(Constants.DOT_CHAR)

                carrier = filename[0:id_hyphen]
                log_date = filename[id_hyphen + 1:id_dot]
                country = os.path.basename(folder)

                path = os.path.abspath(os.path.join(folder, filename))

                id_country_carrier = CountryCarrierDAO.insert_or_retrieve_country_carrier(country, carrier)

                #foo = path.replace('\\\\', '\\') # this line is for windows

                foo = path.replace('/opt/MT_Reports/traffic/', Constants.EMPTY_CHAR)

                exists = ImportedFileDAO.exists_imported_file((id_country_carrier, foo, log_date, ))

                if exists:
                    continue

                id_imported_file = ImportedFileDAO.insert_imported_file((id_country_carrier, foo, log_date, ))

                date_elements = log_date.split(Constants.HYPHEN_CHAR)

                if int(date_elements[0]) < Constants.MIN_YEAR:
                    continue

                with open(path) as f:

                    report_http = []
                    report_smpp = []
                    report_http_summary = []
                    report_smpp_summary = []

                    reader = csv.reader(f, quotechar='\'')

                    for row in reader:

                        if 0 != len(row):

                            if Constants.HOURLY == row[0]:

                                items = [x if Constants.EMPTY_CHAR != x else None for x in row]

                                if Constants.HTTP_XBI_LINE_LEN == len(items):

                                    report_http.append((id_imported_file,items[4],
                                                        items[5], items[6], items[7],
                                                        items[8], items[9], items[10], items[11],
                                                        items[12],))

                                elif Constants.SMPP_LINE_LEN == len(items):

                                    report_smpp.append((
                                        id_imported_file, items[4], items[5], items[6], items[7], items[8],))

                            elif Constants.SUMMARY == row[0]:

                                items = [x if Constants.EMPTY_CHAR != x else None for x in row]

                                if 0 != len(report_http):

                                    report_http_summary.append(self.get_data(id_imported_file, items))

                                elif 0 != len(report_smpp):

                                    report_smpp_summary.append(self.get_data(id_imported_file, items))

                    if 0 != len(report_http):

                        ReportDAO.insert_reports('HTTP_HOURLY', report_http)

                        if 0 != len(report_http_summary):
                            ReportDAO.insert_reports('HTTP_SUMMARY', report_http_summary)

                    elif 0 != len(report_smpp):

                        ReportDAO.insert_reports('SMPP_HOURLY', report_smpp)

                        if 0 != len(report_smpp_summary):
                            ReportDAO.insert_reports('SMPP_SUMMARY', report_smpp_summary)


parser = Parser('/opt/MT_Reports/traffic/data')
parser.generate()